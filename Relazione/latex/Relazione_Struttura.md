# Struttura della relazione

- Le specifiche del sistema di IR, della libreria di valutazione e
altri software/librerie (eventuali) utilizzati

- Tabelle/plot con i risultati sperimentali e breve descrizione
Plot relativi al test statistico

- URL al repository con il codice e i risultati sperimentali
eventuale source code sviluppato dallo studente

- file delle run

- file generati dalla libreria di valutazione

- file dei plot
