# Codice per lo svolgimento dell' Homework 1 di Reperimento dell'Informazione

Autore: **Teofan Clipa**  
Matricola: **1179781**

## Struttura del repo

- **Matlab** contiene i file per l'esecuzione del test statistico e le immagini dei grafici ottenuti dentro la cartella *Matlab/grafici*.

- **Relazione** contiene il file *PDF* della relazione. Dentro la cartella *Relazione/ùlatex* ci sono
tutti i file necessari alla compilazione della stessa.

- **Terrier** contiene i file di configurazione _.proprerties_ utilizzati per le run di *Terrier*. All'interno si trova anche la cartella *Evaliations* che contiene invece
le valutazioni delle misure delle run.

- **res** contiene i file txt dei topic, del ground truth e dei risultati ottenuti dalle run, oltre alle valutazioni delle run eseguite con python.

- **src** contiene i file sorgente usati per la costruzione degli indici, per
l'esecuzione delle run e della valutazione dei sistemi della run di python.

Dentro la maggior parte delle cartelle è presente un file *Readme* che specifica meglio il contenuto delle stesse.
Fare riferimento a quello per ulteriori dettagli.

## Passi per riprodurre i risultati ottenuti

### Nota preliminare

Dentro il repo ci sono già tutti i file ottenuti durante lo svolgimento dell'homework,
non è necessario ricreare tutti gli indici ed eseguire le ricerche per forza.
Riporto i passi che ho seguito per completezza. Se si
vuole vedere solamente i risultati, allora basta entrare nella cartella *Matlab* ed seguire il file
*Readme* presente.

I risultati ottenuti con le run eseguite con python non sono state considerate per lo svolgimento dell'homework. Altri dettagli sono presenti nel
file *Readme* dentro la cartella *src*.

### Requisiti

Per poter riprodurre i risultati ottenuti seguendo i passi sotto
è neccessario aver installato nel proprio computer
- per il calcolo delle valutazioni è necessario aver compilato _trec_eval_, ultima versione disponibile, e di averlo aggiunto alla _PATH_ del proprio sistema
in modo da poterlo richiamare direttamente da riga di comando.
- la collezione di documenti deve essere scompattata e inserita interamente in una cartella del proprio disco.
- _Matlab_ è richiesto per poter eseguire i test statistici e per disegnare i grafici.

- per le run con _python3_ è necessario che le seguenti librerie aggiuntive siano presenti: _gensim_, _whoosh_, _numpy_.

### Riproduzione dei risultati (compreso gli indici)

1.  **Run con Terrier 4.4**:
   - aggiungere la collezione di documenti eseguendo il comando `bin/trec_setup.sh path_documenti`
   - copiare dalla cartella _Terrier_ del repo, a turno, ognuno dei file
   _.properties_ e quindi eseguire per ciascuno `bin/trec_terrier.sh -i` per creare l'indice
   - eseguire `bin/trec_terrier.sh -r -Dtrec.topics=/path_file_topics` per eseguire la ricerca,
   ricordarsi di specificare il modello da riga di comando nel caso di **StoplistStemmerBM25**
   e di **StoplistStemmerTF_IDF** dato che usano lo stesso indice. Il modello
   va specificato con il comando `-Dtrec.model=TF_IDF`.
   - rinominare i file _.res_ ottenuti in base al modello utilizzato, come:
      - StoplistStemmerBM25 >> results1_terrier.txt
      - StoplistStemmerTF_IDF >> results2_terrier.txt
      - NoStoplistStemmerBM25 >> results3_terrier.txt
      - NoStoplistNoStemmerTF_IDF >> results4_terrier.txt
   - a questo punto, dentro la cartella **Terrier/Evaluations** c'è uno script, *Single_evaluations.sh* che chiama **trec_eval** ed esegue la valutazione di *MAP*, *RPrec* e *Precision at 10*. Eseguendolo si otterranno i file
   necessari per il corretto funzionamento dei file di MATLAB.
2. **Analisi con Matlab**:
   - aprire i tre file presenti nella cartella *Matlab* e quindi eseguire il codice, verranno prodotti 3 grafici per file
   - i grafici sono anche presenti come immagini nella cartella *Matlab/grafici*
3. **Run con python e whoosh**:
   - entrare nella cartella _src_ e da terminale eseguire `sh Create_indexes.sh`
   il comando creerà una cartella chiamata __HW1_Indexes__ dove verranno salvati gli indici.
   L'esecuzione di questo comando necessiterà di un paio d'ore
   - una volta creati gli indici, eseguire `sh Execute_search.sh`, questo comando eseguirà la ricerca dei topic
   all'interno degli indici. Il file contenente i topic dovrà essere inserito
   nella cartella _res_ ed essere denominato _Topics.txt_. I risultati della ricerca sono salvati in dei file
   salvati sempre nella cartella _res_ e chiamati con un nome esemplificativo, per es _NoStoplistStemmerBM25.txt_
   nel caso si tratti del file con i risultati ottenuti **non** usando la _stoplist_, usando invece il _porter stemmer_ con il modello __BM25__.
   - accedere alla cartella _res_ ed eseguire il comando `sh Execute_evaluation`. Lo script
   chiamerà __trec_eval__ e gli farà eseguire tutte le misure necessarie. Verranno creati 4 file con i risultati.
   Il nome dei file è _risultati*.txt_. Le corrispondenze tra il file risultati è la seguente:
      - StoplistStemmerBM25.txt >> results1.txt
      - StoplistStemmerTF_IDF.txt >> results2.txt
      - NoStoplistStemmerBM25.txt >> results3.txt
      - NoStoplistNoStemmerTF_IDF.txt >> results4.txt
