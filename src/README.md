# Contenuti

In questa cartella sono presenti i file sorgente *python*. I nomi dei file sorgente
dovrebbero essere auto-esplicativi. Per convenienza, sono presenti anche due
script:

- **Create_indexes.sh** si occupa di eseguire i file che creano gli indici, che
verranno creati in una cartella *HW1_Indexes*. Il processo di creazione è molto
lungo, sul mio computer il tempo necessario supera le 3 ore.

- **Execute_search** sfrutta gli indici creati per eseguire i file che effettuano
la ricerca dei *topic* all'interno della collezione. Ovviamente sono necessari
gli indici per la corretta esecuzione di questo script.

Ulteriori commenti e spiegazioni sono disponibili all'interno del codice sorgente.
Molte funzioni e classi usate all'interno sono state originariamente scritte
dal prof. Gianmaria Silvello nei notebook Jupyter che ci ha reso disponibili.
