# Importiamo le funzioni utili per la creazione degli indici
import Functions as fun
import os, os.path
from whoosh.filedb.filestore import FileStorage
from whoosh import index
from whoosh.fields import Schema, STORED, ID, TEXT
import time
from math import *
from gensim.parsing.preprocessing import preprocess_string

'''
    Definisco una funzione di preprocessing in base all'indice richiesto. In questo
    caso l'indice va fatto con l'uso di stoplist e dello stemmer, si può quindi
    usare direttamente la funzione di preprocessing di gensim che esegue tutte queste
    operazioni.
'''
def preprocess(sentence):
    return preprocess_string(sentence)

# Path utili
collection_path="../Docs/Gasta"
index_path="HW1_indexes"

# Schema usato dal prof. Silvello
schema = Schema(title=TEXT(stored=True), content=TEXT,
                path=ID(stored=True))

exists = index.exists_in(index_path)

if not exists:
    if not os.path.exists(index_path):
        os.mkdir(index_path)

    storage = FileStorage(index_path)

    # Crea l'oggetto indice, assegnandogli un nome significativo
    ix = storage.create_index(schema, indexname="StoplistStemmer")
else :
    ix = storage.open_dir(schema, indexname="StoplistStemmer")

# All'oggetto MyDocuments passo sia il path dei documenti che la funzione da usare per il preprocessing
corpus_memory_friendly = fun.MyDocuments(path=collection_path, preprocess=preprocess)

writer = ix.writer()

# Creazione dell'indice. Codice del prof. Gianmaria Silvello
print("Indexing ...")
start_time = time.time()
i = 0
for doc in corpus_memory_friendly:
    i = i + 1
    if(i % 20000 == 0):
        print("%d docs indexed in %s seconds ---" % (i, time.time() - start_time))
        writer.commit(merge=False)
        writer = ix.writer()

    writer.add_document(title=doc.getTitle(), content=doc.getContent(),
                    path=doc.getTRECid())

print("FINAL: %d docs indexed in %s seconds ---" % (i, time.time() - start_time))

writer.commit(optimize=True)

print("Index merged and optimized in %s seconds ---" % (time.time() - start_time))
