#!/usr/bin/env bash

if find "HW1_indexes" -mindepth 1 -print -quit 2>/dev/null | grep -q .; then
    echo "La cartella contiene già degli indici, rimozione degli indici preesistenti in corso..."
    rm -rf HW1_indexes/*
else
    echo "Cartella già vuota, inizio creazione degli indici..."
fi

echo "Fatto. Passiamo alla creazione dei 3 indici..."

python create_index1.py
echo "Primo indice creato con successo!"
python create_index2.py
echo "Secondo indice creato con successo!"
python create_index3.py
echo "Terzo indice creato con successo!"

echo "Indici creati."
