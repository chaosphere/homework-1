# Contenuti

In questa cartella sono presenti 3 file con esensione *mlx*. In questi file vengono
lette le misure delle valutazioni delle run, presenti dentro la cartella
*../Terrier/Evaluations* e quindi viene eseguito il test **ANOVA-1** tra i 4 sistemi considerati.

La cartella *grafici* contiene tutti i grafici ottenuti dall'esecuzione dei file *Matlab*
per convenienza.

**NOTA**: dentro i file *mlx*, il path che punta ai risultati è **assoluto**, modificarlo prima di
eseguire il codice!

Parte del codice è stato scritto dal prof. Nicola Ferro. Il codice sorgente è
disponibile al seguente link: https://bitbucket.org/frrncl/ir-unipd/src/master/
